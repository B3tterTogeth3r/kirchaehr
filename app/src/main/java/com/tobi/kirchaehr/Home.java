package com.tobi.kirchaehr;

import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;


public class Home extends ActionBarActivity {

    Button einleitung, ablauf, spiele1, spiele2, quiz, goetter, impulse, faq, b1, b2, b3, b4, b5, b6, b7, b8;
    TextView titel, untertitel1, untertitel2, untertitel3, untertitel4, untertitel5, untertitel6, untertitel7, untertitel8, text1, text2, text3, text4, text5, text6, text7, text8;
    int picZeus, picPoseidon, picHades, picHermes, picAthene, picAphrodite, picHera, picArtemis;
    TextView zeitTitel1, zeitTitel2, zeitTitel3, zeit1, zeit2, zeit3;
    ImageView bspBild;
    LinearLayout SubNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        final MediaPlayer mp = MediaPlayer.create(this, R.drawable.quiz_sound);//quiz_sound
        final MediaPlayer einleitung_sound = MediaPlayer.create(this, R.drawable.einleitung_sound);//einleitung_sound
        //ButtonNav
        einleitung = (Button)findViewById(R.id.bEinleitung);
        ablauf = (Button)findViewById(R.id.bAblauf);
        spiele1 = (Button)findViewById(R.id.bSpiele1);
        spiele2 = (Button)findViewById(R.id.bSpiele2);
        quiz = (Button)findViewById(R.id.bQuiz);
        goetter = (Button)findViewById(R.id.bGoetter);
        impulse = (Button)findViewById(R.id.bImpulse);
        faq = (Button)findViewById(R.id.bFAQ);
        //SubNav Layout
        SubNav = (LinearLayout)findViewById(R.id.Unterauswahl);
        //UnterNav Button
        b1 = (Button)findViewById(R.id.b1);
        b2 = (Button)findViewById(R.id.b2);
        b3 = (Button)findViewById(R.id.b3);
        b4 = (Button)findViewById(R.id.b4);
        b5 = (Button)findViewById(R.id.b5);
        b6 = (Button)findViewById(R.id.b6);
        b7 = (Button)findViewById(R.id.b7);
        b8 = (Button)findViewById(R.id.b8);
        //Titel TV
        titel = (TextView)findViewById(R.id.tvTitel);
        //Untertitel TV
        untertitel1 = (TextView)findViewById(R.id.tvUtitel1);
        untertitel2 = (TextView)findViewById(R.id.tvUtitel2);
        untertitel3 = (TextView)findViewById(R.id.tvUtitel3);
        untertitel4 = (TextView)findViewById(R.id.tvUtitel4);
        untertitel5 = (TextView)findViewById(R.id.tvUtitel5);
        untertitel6 = (TextView)findViewById(R.id.tvUtitel6);
        untertitel7 = (TextView)findViewById(R.id.tvUtitel7);
        untertitel8 = (TextView)findViewById(R.id.tvUtitel8);
        //Text TV
        text1 = (TextView)findViewById(R.id.tvtext1);
        text2 = (TextView)findViewById(R.id.tvtext2);
        text3 = (TextView)findViewById(R.id.tvtext3);
        text4 = (TextView)findViewById(R.id.tvtext4);
        text5 = (TextView)findViewById(R.id.tvtext5);
        text6 = (TextView)findViewById(R.id.tvtext6);
        text7 = (TextView)findViewById(R.id.tvtext7);
        text8 = (TextView)findViewById(R.id.tvtext8);
        //Bilder
        bspBild = (ImageView)findViewById(R.id.gottbild);
        picZeus = R.drawable.zeus;
        picPoseidon = R.drawable.poseidon;
        picHades = R.drawable.hades;
        picHermes = R.drawable.hermes;
        picAthene = R.drawable.athene;
        picAphrodite = R.drawable.aphrodite;
        picHera = R.drawable.hera;
        picArtemis = R.drawable.artemis;
        bspBild.setVisibility(View.GONE);
        //Zeitplan TV
        zeitTitel1 = untertitel1;
        zeitTitel2 = (TextView)findViewById(R.id.tvPlanTitel2);
        zeitTitel3 = (TextView)findViewById(R.id.tvPlanTitel3);
        zeit1 = text1;
        zeit2 = (TextView)findViewById(R.id.tvPlan2);
        zeit3 = (TextView)findViewById(R.id.tvPlan3);

        //Einleitungstext
        einleitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Einleitung
                reset();
                SubNav.setVisibility(View.VISIBLE);
                b1.setVisibility(View.VISIBLE);
                b1.setText(R.string.audio_Einleitung);
                titel.setVisibility(View.VISIBLE);
                titel.setText(R.string.titelEinleitung);
                text1.setVisibility(View.VISIBLE);
                text1.setText(R.string.textEinleitung);
                text2.setVisibility(View.VISIBLE);
                //text2.setText(R.string.textEinleitung_ergaenzung);
                b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    einleitung_sound.start();
                }
            });
            }
        });

        ablauf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO Ablaufanzeige einrichten
                reset();
                navleisteON();
                b1.setText(R.string.freitag);
                b2.setText(R.string.samstag);
                b3.setText(R.string.sonntag);
                b4.setVisibility(View.GONE);
                zeitTitel1.setVisibility(View.VISIBLE);
                zeitTitel2.setVisibility(View.VISIBLE);
                zeitTitel3.setVisibility(View.VISIBLE);
                zeitTitel1.setText(R.string.zeit1Zeit);
                zeitTitel2.setText(R.string.zeit2Zeit);
                zeitTitel3.setText(R.string.zeit3Zeit);
                zeit1.setVisibility(View.VISIBLE);
                zeit2.setVisibility(View.VISIBLE);
                zeit3.setVisibility(View.VISIBLE);

                //Freitag
                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.freitag);
                        zeit1.setText(R.string.zeit1Fr);
                        zeit2.setText(R.string.zeit2Fr);
                        zeit3.setText(R.string.zeit3Fr);
                    }
                });

                //Samstag
                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.samstag);
                        zeit1.setText(R.string.zeit1Sa);
                        zeit2.setText(R.string.zeit2Sa);
                        zeit3.setText(R.string.zeit3Sa);
                    }
                });

                //Sonntag
                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.sonntag);
                        zeit1.setText(R.string.zeit1So);
                        zeit2.setText(R.string.zeit2So);
                        zeit3.setText(R.string.zeit3So);
                    }
                });

            }
        });

        spiele1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Spielbereich 1
                reset();
                navleisteON();
                b5.setVisibility(View.VISIBLE);
                text1.setVisibility(View.VISIBLE);
                b1.setText(R.string.vormittag1Name);
                b2.setText(R.string.vormittag2Name);
                b3.setText(R.string.vormittag3Name);
                b4.setText(R.string.vormittag4Name);
                b5.setText(R.string.vormittagLauf);

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.vormittag1Name);
                        text1.setText(R.string.vormittag1Erkl);
                        text2.setText("");
                        text3.setText("");
                        text4.setText("");
                    }
                });

                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.vormittag2Name);
                        text1.setText(R.string.vormittag2Erkl);
                        text2.setText("");
                        text3.setText("");
                        text4.setText("");
                    }
                });

                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.vormittag3Name);
                        text1.setText(R.string.vormittag3Erkl);
                        text2.setText("");
                        text3.setText("");
                        text4.setText("");
                    }
                });

                b4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.vormittag4Name);
                        text1.setText(R.string.vormittag4Erkl);
                        text2.setText("");
                        text3.setText("");
                        text4.setText("");
                    }
                });

                b5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.vormittagLauf);
                        text1.setText(R.string.vormittagLauf1);
                        text2.setVisibility(View.VISIBLE);
                        text2.setText(R.string.vormittagLauf2);
                        text3.setVisibility(View.VISIBLE);
                        text3.setText(R.string.vormittagLauf3);
                        text4.setVisibility(View.VISIBLE);
                        text4.setText(R.string.vormittagLauf4);
                    }
                });
            }
        });

        spiele2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Spielbereich 2
                reset();
                navleisteON();
                b5.setVisibility(View.VISIBLE);
                b6.setVisibility(View.VISIBLE);
                text1.setVisibility(View.VISIBLE);
                b1.setText(R.string.ranglisteName);
                b2.setText(R.string.ranglisteOstRug);
                b3.setText(R.string.ranglisteBliMath);
                b4.setText(R.string.ranglisteSchiff);
                b5.setText(R.string.ranglisteHase);
                b6.setText(R.string.chaos);

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.ranglisteName);
                        text1.setText(R.string.ranglisteErkl);
                    }
                });

                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.ranglisteOstRug);
                        text1.setText(R.string.ranglisteOstRugErkl);
                    }
                });

                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.ranglisteBliMath);
                        text1.setText(R.string.ranglisteBliMathErkl);
                    }
                });

                b4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.ranglisteSchiff);
                        text1.setText(R.string.ranglisteSchiffErkl);
                    }
                });

                b5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.ranglisteHase);
                        text1.setText(R.string.ranglisteHaseErkl);
                    }
                });

                b6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.chaos);
                        text1.setText(R.string.chaosErkl);
                    }
                });

            }
        });

        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Quiz einrichten
                reset();
                SubNav.setVisibility(View.VISIBLE);
                b1.setVisibility(View.VISIBLE);
                b1.setText(R.string.quizAudio);
                titel.setVisibility(View.VISIBLE);
                titel.setText(R.string.quiztitel);
                text1.setVisibility(View.VISIBLE);
                text2.setVisibility(View.VISIBLE);
                text1.setText(R.string.quiztext1);
                text2.setText(R.string.quiztext2);

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            mp.start();
                    }
                });


            }
        });

        goetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Göttererklärung
                reset();
                navleisteOnAll();
                bspBild.setVisibility(View.VISIBLE);
                b1.setText(R.string.zeus);
                b2.setText(R.string.poseidon);
                b3.setText(R.string.hades);
                b4.setText(R.string.hermes);
                b5.setText(R.string.athene);
                b6.setText(R.string.aphrodite);
                b7.setText(R.string.artemis);
                b8.setText(R.string.hera);
                untertitel1.setText(R.string.bereich);
                untertitel2.setText(R.string.zeichen);
                untertitel3.setText(R.string.uebersicht);
                untertitel4.setText(R.string.huldigung);

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picZeus);
                        titel.setText(R.string.zeus);
                        text1.setText(R.string.zeus1);
                        text2.setText(R.string.zeus2);
                        text3.setText(R.string.zeus3);
                        untertitel4.setVisibility(View.VISIBLE);
                        text4.setText(R.string.zeus4);
                    }
                });

                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picPoseidon);
                        titel.setText(R.string.poseidon);
                        text1.setText(R.string.poseidon1);
                        text2.setText(R.string.poseidon2);
                        text3.setText(R.string.poseidon3);
                        untertitel4.setVisibility(View.VISIBLE);
                        text4.setText(R.string.poseidon4);
                    }
                });

                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picHades);
                        titel.setText(R.string.hades);
                        text1.setText(R.string.hades1);
                        text2.setText(R.string.hades2);
                        text3.setText(R.string.hades3);
                        untertitel4.setVisibility(View.VISIBLE);
                        text4.setText(R.string.hades4);
                    }
                });

                b4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picHermes);
                        titel.setText(R.string.hermes);
                        text1.setText(R.string.hermes1);
                        text2.setText(R.string.hermes2);
                        text3.setText(R.string.hermes3);
                        untertitel4.setVisibility(View.GONE);
                        text4.setText("");
                    }
                });

                b5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picAthene);
                        titel.setText(R.string.athene);
                        text1.setText(R.string.athene1);
                        text2.setText(R.string.athene2);
                        text3.setText(R.string.athene3);
                        untertitel4.setVisibility(View.VISIBLE);
                        text4.setText(R.string.athene4);
                    }
                });

                b6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picAphrodite);
                        titel.setText(R.string.aphrodite);
                        text1.setText(R.string.aphrodite1);
                        text2.setText(R.string.aphrodite2);
                        text3.setText(R.string.aphrodite3);
                        untertitel4.setVisibility(View.GONE);
                        text4.setText("");
                    }
                });

                b7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picArtemis);
                        titel.setText(R.string.artemis);
                        text1.setText(R.string.artemis1);
                        text2.setText(R.string.artemis2);
                        text3.setText(R.string.artemis3);
                        untertitel4.setVisibility(View.GONE);
                        text4.setText("");
                    }
                });

                b8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        text4();
                        bspBild.setImageResource(picHera);
                        titel.setText(R.string.hera);
                        text1.setText(R.string.hera1);
                        text2.setText(R.string.hera2);
                        text3.setText(R.string.hera3);
                        untertitel4.setVisibility(View.GONE);
                        text4.setText("");
                    }
                });
            }
        });

        impulse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Impulse
                reset();
                navleisteON();
                b4.setVisibility(View.GONE);
                b1.setText(R.string.morgenimpulsSa1);
                b2.setText(R.string.abschlussimpulsSo1);
                b3.setText(R.string.ersatzimpuls1);
                text1.setVisibility(View.VISIBLE);
                text2.setVisibility(View.VISIBLE);
                titel.setText(R.string.morgenimpulsSa1);
                text1.setText(R.string.morgenimpulsSa2);
                text2.setText(R.string.morgenimpulsSaGebet);

                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.morgenimpulsSa1);
                        text1.setText(R.string.morgenimpulsSa2);
                        text2.setText(R.string.morgenimpulsSaGebet);
                    }
                });

                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.abschlussimpulsSo1);
                        text1.setText(R.string.abschlussimpulsSo2);
                        text2.setText(R.string.abschlussimpulsSoGebet);
                    }
                });

                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        titel.setText(R.string.ersatzimpuls1);
                        text1.setText(R.string.ersatzimpuls2);
                        text2.setText(R.string.ersatzimpulsGebet);
                    }
                });

            }
        });

        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //FAQ
                reset();
                titel.setVisibility(View.VISIBLE);
                titel.setText(R.string.faqt);
                untertitel1.setVisibility(View.VISIBLE);
                untertitel1.setText(R.string.faqf1);
                text1.setVisibility(View.VISIBLE);
                text1.setText(R.string.faqa1);
                untertitel2.setVisibility(View.VISIBLE);
                untertitel2.setText(R.string.faqf2);
                text2.setVisibility(View.VISIBLE);
                text2.setText(R.string.faqa2);
                untertitel3.setVisibility(View.VISIBLE);
                untertitel3.setText(R.string.faqf3);
                text3.setVisibility(View.VISIBLE);
                text3.setText(R.string.faqa3);
                untertitel4.setVisibility(View.VISIBLE);
                untertitel4.setText(R.string.faqf4);
                text4.setVisibility(View.VISIBLE);
                text4.setText(R.string.faqa4);
                untertitel5.setVisibility(View.VISIBLE);
                untertitel5.setText(R.string.faqf5);
                text5.setVisibility(View.VISIBLE);
                text5.setText(R.string.faqa5);
                untertitel6.setVisibility(View.VISIBLE);
                untertitel6.setText(R.string.faqf6);
                text6.setVisibility(View.VISIBLE);
                text6.setText(R.string.faqa6);
                untertitel7.setVisibility(View.VISIBLE);
                untertitel7.setText(R.string.faqf7);
                text7.setVisibility(View.VISIBLE);
                text7.setText(R.string.faqa7);
                untertitel8.setVisibility(View.VISIBLE);
                untertitel8.setText(R.string.faqf8);
                text8.setVisibility(View.VISIBLE);
                text8.setText(R.string.faqa8);
            }
        });


    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    protected void navleisteON(){
        SubNav.setVisibility(View.VISIBLE);
        b1.setVisibility(View.VISIBLE);
        b2.setVisibility(View.VISIBLE);
        b3.setVisibility(View.VISIBLE);
        b4.setVisibility(View.VISIBLE);
        titel.setVisibility(View.VISIBLE);
    }

    protected void navleisteOnAll(){
        SubNav.setVisibility(View.VISIBLE);
        b1.setVisibility(View.VISIBLE);
        b2.setVisibility(View.VISIBLE);
        b3.setVisibility(View.VISIBLE);
        b4.setVisibility(View.VISIBLE);
        b5.setVisibility(View.VISIBLE);
        b6.setVisibility(View.VISIBLE);
        b7.setVisibility(View.VISIBLE);
        b8.setVisibility(View.VISIBLE);
        titel.setVisibility(View.VISIBLE);
    }

    protected void text4(){
        titel.setVisibility(View.VISIBLE);
        untertitel1.setText(R.string.bereich);
        untertitel1.setVisibility(View.VISIBLE);
        untertitel1.setText(R.string.zeichen);
        untertitel2.setVisibility(View.VISIBLE);
        untertitel1.setText(R.string.uebersicht);
        untertitel3.setVisibility(View.VISIBLE);
        untertitel1.setText(R.string.huldigung);
        untertitel4.setVisibility(View.VISIBLE);
        text1.setVisibility(View.VISIBLE);
        text2.setVisibility(View.VISIBLE);
        text3.setVisibility(View.VISIBLE);
        text4.setVisibility(View.VISIBLE);
    }

    protected void reset(){
        //TV Leeren
        titel.setText("");
        untertitel1.setText("");
        untertitel2.setText("");
        untertitel3.setText("");
        untertitel4.setText("");
        untertitel5.setText("");
        untertitel6.setText("");
        untertitel7.setText("");
        untertitel8.setText("");
        text1.setText("");
        text2.setText("");
        text3.setText("");
        text4.setText("");
        text5.setText("");
        text6.setText("");
        text7.setText("");
        text8.setText("");
        b1.setText("");
        b2.setText("");
        b3.setText("");
        b4.setText("");
        b5.setText("");
        b6.setText("");
        b7.setText("");
        b8.setText("");
        zeitTitel1.setText("");
        zeitTitel2.setText("");
        zeitTitel3.setText("");
        zeit1.setText("");
        zeit2.setText("");
        zeit3.setText("");
        //Unsichtbar machen
        SubNav.setVisibility(View.GONE);
        bspBild.setVisibility(View.GONE);
        untertitel1.setVisibility(View.GONE);
        untertitel2.setVisibility(View.GONE);
        untertitel3.setVisibility(View.GONE);
        untertitel4.setVisibility(View.GONE);
        untertitel5.setVisibility(View.GONE);
        untertitel6.setVisibility(View.GONE);
        untertitel7.setVisibility(View.GONE);
        untertitel8.setVisibility(View.GONE);
        text1.setVisibility(View.GONE);
        text2.setVisibility(View.GONE);
        text3.setVisibility(View.GONE);
        text4.setVisibility(View.GONE);
        text5.setVisibility(View.GONE);
        text6.setVisibility(View.GONE);
        text7.setVisibility(View.GONE);
        text8.setVisibility(View.GONE);
        b1.setVisibility(View.GONE);
        b2.setVisibility(View.GONE);
        b3.setVisibility(View.GONE);
        b4.setVisibility(View.GONE);
        b5.setVisibility(View.GONE);
        b6.setVisibility(View.GONE);
        b7.setVisibility(View.GONE);
        b8.setVisibility(View.GONE);
        zeitTitel1.setVisibility(View.GONE);
        zeitTitel2.setVisibility(View.GONE);
        zeitTitel3.setVisibility(View.GONE);
        zeit1.setVisibility(View.GONE);
        zeit2.setVisibility(View.GONE);
        zeit3.setVisibility(View.GONE);
    }
}